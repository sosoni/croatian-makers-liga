CML.controller('HomeCtrl', function (
    $log,
    $scope,
    CMLConfig,
    institutions,
    CMLUserSvc,
    CMLModalSrv,
    Notify
) {
    let config = CMLConfig;
    let vm = this;
    let password;

    let init = function () {
        vm.config = config;
        vm.data = getEmptyFormData();
        vm.logged = CMLUserSvc.user.logged;
    };

    let getEmptyFormData = function () {
        return {
            name: '',
            address: '',
            postcode: '',
            city: '',
            country: '',
            oib: '',
            director: '',
            directorEmail: '',
            directorPhone: '',
            isInterestedInOrganisation: false,
            email: '', // application user email
            password: '',
            repeatedPassword: '',
            selectableCountry: config.company.country
        };
    };

    vm.resetModal = function () {
        vm.data = getEmptyFormData();
        $scope.registrationForm.$setPristine();
    };

    vm.register = function () {
        if ($scope.registrationForm.$invalid || vm.data.password !== vm.data.repeatedPassword) {
            $scope.registrationForm.$setSubmitted();
            return false;
        }

        // Send password hash over network
        vm.data.password = md5(vm.data.password);
        vm.data.repeatedPassword = md5(vm.data.repeatedPassword);

        institutions.register(vm.data).then(function (response) {
            CMLModalSrv.closeModal('register-modal');
            vm.resetModal();
            Notify.success('Zahtjev za registracijom podnesen.');
            $log.debug('Successful registration: ', response);
        }).catch(function (err) {
            vm.data.password = '';
            vm.data.repeatedPassword = '';
            Notify.warn('Greška pri registraciji!');
            $log.warn('Unsuccessful registration: ', err);
        });
    };

    init();
});