CML.factory('userInstitution', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get institution data
    let get = function (params) {
        return $http.get(config.api + '/institutions/user/list', {
            params: params
        });
    };

    return {
        get: get
    };
});