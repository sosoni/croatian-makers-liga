CML.controller('UserInstitutionCtrl', function (
    CMLConfig,
    CMLUserSvc,
    userInstitution
) {
    let vm = this;
    let config = CMLConfig;
    let user = CMLUserSvc.user;

    let init = function () {
        userInstitution.get({ institutionId: user.data.institutionId }).then(function(response) {
            vm.data = response.data;
        });
    };

    init();
});