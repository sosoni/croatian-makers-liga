CML.controller('UserRoundsCtrl', function (
    $filter,
    $log,
    CMLConfig,
    CMLUserSvc,
    Notify,
    userRounds
) {
    let vm = this;
    let config = CMLConfig;
    let user = CMLUserSvc.user;

    let init = function () {
        vm.config = config;
        vm.search = {
            name: '',
            round: {
                name: '',
                key: ''
            }
        };

        // Populate data
        getData();
    };

    let getData = function () {
        userRounds.get({ institutionId: user.data.institutionId }).then(function (response) {
            parseData(response.data);

            vm.filters = getFilters();
            vm.sortBy(vm.sortType || vm.filters[0].key);
        });
    };

    let parseData = function (data) {
        // Modify data
        vm.data = data.rounds;
        vm.roundsMap = data.roundsMap;
        vm.roundsTypes = data.roundsTypes;
        vm.mentors = data.mentors;
        vm.competitors = data.competitors.map(function(competitor) {
            competitor.selected = false;
            return competitor;  
        });
        vm.institution = data.institution;
        vm.regionalCenterNames = data.regionalCenterNames;
        vm.page = vm.page || 1;
    };

    // Get predefined filters for ordering data
    let getFilters = function () {
        return [{
            name: 'Naziv',
            key: 'name'
        }, {
            name: 'Status',
            key: 'opened'
        }];
    };

    vm.selectMentor = function (mentor, ageGroup) {
        switch (ageGroup) {
            case 1:
                vm.modal.selectedYoungerMentor = mentor;
                vm.modal.selectedYoungerMentor.compeatingGroup = ageGroup;
                break;
            case 2:
                vm.modal.selectedOlderMentor = mentor;
                vm.modal.selectedOlderMentor.compeatingGroup = ageGroup;
                break;
        }
    };

    vm.addCompetitor = function (competitor, ageGroup) {
        switch (ageGroup) {
            case 1:
                if (vm.modal.selectedYoungerCompetitors.length >= 2) {
                    Notify.warn('Možete dodati najviše 2 natjecatelja!');
                    return;
                }
                vm.modal.selectedYoungerCompetitors.push(competitor);
                break;
            case 2:
                if (vm.modal.selectedOlderCompetitors.length >= 2) {
                    Notify.warn('Možete dodati najviše 2 natjecatelja!');
                    return;
                }
                vm.modal.selectedOlderCompetitors.push(competitor);
                break;
        }

        competitor.selected = true;
        competitor.compeatingGroup = ageGroup;
    };

    vm.removeCompetitor = function (competitor, ageGroup) {
        let competitors = ageGroup === 1 ?
            vm.modal.selectedYoungerCompetitors : vm.modal.selectedOlderCompetitors;

        for (let i = 0; i < competitors.length; i++) {
            let c = competitors[i];
            if (c.id === competitor.id) {
                competitor.selected = false;
                competitor.compeatingGroup = null;
                competitors.splice(i, 1);
                break;
            }
        }
    };

    // Update data of modal object
    vm.updateModalData = function (round, type) {
        vm.modal = round;

        // Set modal round data
        vm.modal.selectedAgeGroup = 1;
        vm.modal.selectedYoungerCompetitors = [];
        vm.modal.selectedOlderCompetitors = [];

        let roundMap = vm.roundsMap.find(function (roundMap) {
            return roundMap.roundId === round.id;
        });
        if (roundMap) {
            vm.competitors.forEach(function (competitor) {
                if (roundMap.competitors[0] && (competitor.id === roundMap.competitors[0].id)) {
                    competitor.selected = true;
                    competitor.compeatingGroup = roundMap.competitors[0].compeatingGroup;
                    vm.modal.selectedYoungerCompetitors.push(competitor);
                } else if (roundMap.competitors[1] && (competitor.id === roundMap.competitors[1].id)) {
                    competitor.selected = true;
                    competitor.compeatingGroup = roundMap.competitors[1].compeatingGroup;
                    vm.modal.selectedYoungerCompetitors.push(competitor);
                } else if (roundMap.competitors[2] && (competitor.id === roundMap.competitors[2].id)) {
                    competitor.selected = true;
                    competitor.compeatingGroup = roundMap.competitors[2].compeatingGroup;
                    vm.modal.selectedOlderCompetitors.push(competitor);
                } else if (roundMap.competitors[3] && (competitor.id === roundMap.competitors[3].id)) {
                    competitor.selected = true;
                    competitor.compeatingGroup = roundMap.competitors[3].compeatingGroup;
                    vm.modal.selectedOlderCompetitors.push(competitor);
                } else {
                    competitor.selected = false;
                }
            });
        }

        // Fizičko kolo
        if (type === 2) {
            vm.modal.regionalCenterName = vm.institution.regionalCenterName;

            if (roundMap) {
                vm.modal.selectedYoungerMentor = vm.mentors.find(function (mentor) {
                    if (roundMap.mentors[0] && (mentor.id === roundMap.mentors[0].id)) {
                        mentor.compeatingGroup = roundMap.mentors[0].compeatingGroup;
                        return true;
                    }
                });
                vm.modal.selectedOlderMentor = vm.mentors.find(function (mentor) {
                    if (roundMap.mentors[1] && (mentor.id === roundMap.mentors[1].id)) {
                        mentor.compeatingGroup = roundMap.mentors[1].compeatingGroup;
                        return true;
                    }
                });
            }
        }
    };

    // Sort data by filter type
    vm.sortBy = function (type, reverse) {
        vm.sortType = type;
        vm.data = $filter('orderBy')(vm.data, type, reverse);
    };

    // Post data to db
    vm.update = function () {
        let data = {
            roundId: vm.modal.id,
            institutionId: user.data.institutionId,
            regionalCenterName: vm.institution.isRegionalCenter ? vm.institution.regionalCenterName : vm.modal.regionalCenterName,
            mentors: [vm.modal.selectedYoungerMentor, vm.modal.selectedOlderMentor],
            competitors: [
                vm.modal.selectedYoungerCompetitors[0],
                vm.modal.selectedYoungerCompetitors[1],
                vm.modal.selectedOlderCompetitors[0],
                vm.modal.selectedOlderCompetitors[1]
            ]
        };

        userRounds.update(data).then(function (response) {
            if (response.data) {
                getData();
                Notify.success('Uspješno ste editirali natjecatelje.');
                $log.debug('Editing competitors: ', response);
            }
            vm.modal = null;
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja natjecatelja!');
            $log.warn(err);
        });
    };

    init();
});