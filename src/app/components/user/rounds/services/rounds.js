CML.factory('userRounds', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get rounds
    let get = function (params) {
        return $http.get(config.api + '/rounds/user/list', {
            params: params
        });
    };

    let update = function (data) {
        return $http.put(config.api + '/rounds/user/update', data);
    };

    return {
        get: get,
        update: update
    };
});