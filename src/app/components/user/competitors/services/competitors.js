CML.factory('userCompetitors', function (
    $http,
    Upload,
    CMLConfig
) {
    let config = CMLConfig;

    // Get competitors
    let get = function (params) {
        return $http.get(config.api + '/competitors/user/list', {
            params: params
        });
    };

    // Update competitors
    let update = function (data) {
        return $http.put(config.api + '/competitors/user/update', data);
    };

    let create = function (data) {
        return $http.post(config.api + '/competitors/user/create', data);
    };

    return {
        get: get,
        update: update,
        create: create
    };
});