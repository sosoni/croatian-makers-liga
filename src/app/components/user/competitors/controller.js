CML.controller('UserCompetitorsCtrl', function (
    $filter,
    $log,
    CMLConfig,
    CMLModalSrv,
    CMLUserSvc,
    Notify,
    userCompetitors
) {
    let vm = this;
    let config = CMLConfig;
    let user = CMLUserSvc.user;

    let init = function () {
        vm.config = config;
        vm.user = {};
        vm.search = {
            name: '',
            region: ''
        };

        vm.newUser = {
            birthdate: new Date(),
            institutionId: user.data.institutionId
        };

        vm.newUserTemplate = angular.copy(vm.newUser);
        getCompetitors();
    };

    let getCompetitors = function () {
        userCompetitors.get({ institutionId: user.data.institutionId }).then(function (response) {
            vm.data = response.data.competitors;
            vm.ageGroups = response.data.competitorsAgeGroups;
            vm.data.birthdate = new Date(vm.data.birthdate);
            vm.page = vm.page || 1;

            vm.filters = getFilters();
            vm.sortBy(vm.sortType || vm.filters[0].key);
        });
    };

    // Get predefined filters for ordering data
    let getFilters = function () {
        return [{
            name: 'Ime',
            key: 'name'
        }, {
            name: 'Prezime',
            key: 'lastname'
        }, {
            name: 'Datum rođenja',
            key: 'birthdate'
        }, {
            name: 'Razred',
            key: 'class'
        }, {
            name: 'Spol',
            key: 'sex'
        }];
    };

    vm.updateUserData = function (id) {
        let user = $filter('filter')(vm.data, { id: id }, true);
        vm.user = user[0];
        vm.user.birthdate = new Date(vm.user.birthdate);
    };

    // Sort data by filter type
    vm.sortBy = function (type, reverse) {
        vm.sortType = type;
        vm.data = $filter('orderBy')(vm.data, type, reverse);
    };

    vm.editUser = function () {
        userCompetitors.update(vm.user).then(function (response) {
            if (response.data) {
                getCompetitors();
                CMLModalSrv.closeModal('edit-competitor');
                Notify.success('Uspješno ste uredili natjecatelja.');
                $log.debug('Updating competitors: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja natjecatelja!');
            $log.warn(err);
        });
    };

    vm.createUser = function () {
        if (vm.createUserForm.$invalid) {
            vm.createUserForm.$setSubmitted();
            return false;
        }

        userCompetitors.create(vm.newUser).then(function (response) {
            if (response.data) {
                CMLModalSrv.closeModal('create-competitor');
                vm.resetCreateModal();
                getCompetitors();
                Notify.success('Uspješno ste dodali natjecatelja.');
                $log.debug('Creating competitors: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom dodavanja natjecatelja!');
            $log.warn(err);
        });
    };

    vm.resetCreateModal = function () {
        vm.newUser = angular.copy(vm.newUserTemplate);
        vm.createUserForm.$setPristine();
    };

    init();
});