CML.controller('UserMentorsCtrl', function (
    $filter,
    $log,
    $scope,
    CMLConfig,
    CMLUserSvc,
    CMLModalSrv,
    Notify,
    userMentors
) {
    let vm = this;
    let config = CMLConfig;
    let user = CMLUserSvc.user;

    let init = function () {
        vm.config = config;
        vm.search = {
            name: '',
            region: ''
        };

        // Populate data
        getMentors();
    };

    let getMentors = function () {
        userMentors.get({ institutionId: user.data.institutionId }).then(function (response) {
            vm.data = response.data.mentors;
            vm.ageGroups = response.data.mentorsAgeGroups;
            vm.page = vm.page || 1;

            vm.filters = getFilters();
            vm.sortBy(vm.sortType || vm.filters[0].key);
        });
    };

    // Get predefined filters for ordering data
    let getFilters = function () {
        return [{
            name: 'Naziv',
            key: 'name'
        }];
    };

    // Update data of modal object
    vm.updateModalData = function (type, mentor) {
        switch (type) {
            case 'create':
                vm.modal = {};
                break;
            case 'edit':
                vm.modal = mentor;
                break;
        }
    };

    // Sort data by filter type
    vm.sortBy = function (type, reverse) {
        vm.sortType = type;
        vm.data = $filter('orderBy')(vm.data, type, reverse);
    };

    // Post data to database
    vm.postData = function () {
        userMentors.update(vm.modal).then(function (response) {
            if (response.data) {
                getMentors(); // Update view data
                Notify.success('Uspješno ste editirali mentora.');
                $log.debug('Updating mentors: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja mentora!');
            $log.warn(err);
        });
        vm.modal = null;
    };

    // Create data to database
    vm.createData = function () {
        if ($scope.creationForm.$invalid) {
            $scope.creationForm.$setSubmitted();
            return false;
        }

        vm.modal.institutionId = user.data.institutionId;
        userMentors.create(vm.modal).then(function (response) {
            if (response.data) {
                getMentors(); // Update view data
                CMLModalSrv.closeModal('create-modal');
                Notify.success('Uspješno ste dodali mentora.');
                $log.debug('Creating mentors: ', response);
            }
            vm.modal = null;
        }).catch(function (err) {
            Notify.warn('Greška prilikom dodavanja mentora!');
            $log.warn(err);
        });
    };

    init();
});