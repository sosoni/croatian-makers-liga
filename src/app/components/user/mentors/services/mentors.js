CML.factory('userMentors', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get mentors
    let get = function (params) {
        return $http.get(config.api + '/mentors/user/list', {
            params: params
        });
    };

    // Update mentors
    let update = function (data) {
        return $http.put(config.api + '/mentors/user/update', data);
    };

    // Create mentor
    let create = function (data, institutionId) {
        return $http.post(config.api + '/mentors/user/create', data);
    };

    return {
        get: get,
        update: update,
        create: create
    };
});