CML.controller('UserMaterialsCtrl', function (
    CMLMaterialsSvc,
    CMLUserSvc,
    CMLConfig,
    Notify
) {
    let vm = this;
    let user = CMLUserSvc.user;

    let init = function () {
        vm.config = CMLConfig;
        vm.page = vm.page || 1;
        vm.uploadedMaterials = [];
        getMaterials();
    };

    let getMaterials = function () {
        // CMLMaterialsSvc.getMaterials({ institutionId: user.data.institutionId })
        CMLMaterialsSvc.getMaterials().then(function (response) {
            vm.uploadedMaterials = response.data;
        });
    };

    vm.downloadMaterial = function (material) {
        CMLMaterialsSvc.downloadMaterial(material.id).then(function (response) {
            let file = new Blob([response.data], { type: material.mime_type });
            saveAs(file, material.name);
        }).catch(function (err) {
            Notify.warn('Greška prilikom skidanja materijala!');
            $log.warn(err);
        });
    };

    vm.getFileSize = function (sizeInBytes) {
        return (sizeInBytes / (1000*1024)).toFixed(2);
    };

    init();
});