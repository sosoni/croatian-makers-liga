CML.factory('userResults', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get results
    let get = function () {
        return $http.get(config.api + '/results/list');
    };

    return {
        get: get
    };
});