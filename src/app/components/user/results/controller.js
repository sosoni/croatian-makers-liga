CML.controller('UserResultsCtrl', function (
    userResults,
    CMLConfig
) {
    let vm = this;
    let config = CMLConfig;

    let init = function () {
        vm.config = config;
        vm.search = { name: '' };

        getResults();
    };

    let getResults = function () {
        userResults.get().then(function (response) {
            vm.data = response.data;
        });
    };

    init();
});