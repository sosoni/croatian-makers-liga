CML.controller('BaseCtrl', function (
    $location,
    $log,
    $scope,
    $state,
    CMLConfig,
    CMLUserSvc,
    Notify,
    CMLModalSrv
) {
    let config = CMLConfig;
    let userService = CMLUserSvc;
    let vm = this;
    let userType;

    let init = function () {
        // Define userTypes
        userType = {
            admin: 'admin',
            user: 'user'
        };

        vm.user = userService.user;
        vm.isSupportEnabled = config.support.enabled;
        vm.resetEmail = '';
        vm.forgotPassActivated = false;
    };

    // Get UI view based on user type
    let getUIMenu = function (type) {
        let ui = [];

        switch (type) {
            case userType.admin:
                ui = [{
                    name: 'Ustanove',
                    url: '#/admin/institutions'
                },
                {
                    name: 'Mentori',
                    url: '#/admin/mentors'
                },
                {
                    name: 'Natjecatelji',
                    url: '#/admin/competitors'
                },
                {
                    name: 'Kola lige',
                    url: '#/admin/rounds'
                },
                {
                    name: 'Materijali',
                    url: '#/admin/materials'
                },
                {
                    name: 'Rezultati',
                    url: '#/admin/results'
                }];
                break;
            case userType.user:
                ui = [{
                    name: 'Ustanova',
                    url: '#/user/institution'
                },{
                    name: 'Mentori',
                    url: '#/user/mentors'
                },
                {
                    name: 'Natjecatelji',
                    url: '#/user/competitors'
                },
                {
                    name: 'Kola lige',
                    url: '#/user/rounds'
                },
                {
                    name: 'Materijali',
                    url: '#/user/materials'
                },
                {
                    name: 'Rezultati',
                    url: '#/user/results'
                }];
                break;
        }

        return ui;
    };

    // Go to default starting state
    let navigateDefaultState = function (type) {
        // Go to default state only from home page
        if ($state.current.name !== 'home') return;

        switch (type) {
            case userType.admin:
                $state.go('admin-institutions');
                break;
            case userType.user:
                $state.go('user-institution');
                break;
        }
    };

    // Set active class
    vm.pageClass = function (path) {
        return path === '#' + $location.path() ? 'active' : '';
    };

    // Login action
    vm.login = function () {
        if ($scope.loginForm.$invalid) {
            $scope.loginForm.$setSubmitted();
            return false;
        }
        userService.login({
            email: vm.email,
            password: md5(vm.password)
        }).then(function (response) {
            Notify.success('Uspješno ste se logirali.');
            CMLModalSrv.closeModal('login-modal');
            vm.resetModal();
            $log.debug('Login: ', response);
        }).catch(function (err) {
            Notify.warn('Greška pri logiranju.');
            $log.warn('Login: ', err);
        });
    };

    // Logout action
    vm.logout = function () {
        userService.logout().then(function (response) {
            $log.debug('Logout: ', response);
        }).catch(function (err) {
            $log.warn('Logout: ', err);
        });
    };

    // Reset password action
    vm.resetPassword = function () {
        if ($scope.forgotPass.$invalid) {
            $scope.forgotPass.$setSubmitted();
            return false;
        }
        let data = {
            href: window.location.href + 'password-reset?uuid=',
            email: vm.resetEmail
        };

        CMLUserSvc.resetPassword(data).then(function () {
            Notify.success('Zahtjev za resetom uspješno podnesen');
            CMLModalSrv.closeModal('login-modal');
            vm.resetModal();
        }).catch(function (err) {
            Notify.warn('Greška pri reset-u lozinke');
        });
    };

    vm.resetModal = function () {
        vm.email = '';
        vm.password = '';
        vm.resetEmail = '';
        vm.forgotPassActivated = false;
        $scope.forgotPass.$setPristine();
        $scope.loginForm.$setPristine();
    };

    // Navigate to correct state and build ui menu
    $scope.$watch(function () {
        return vm.user.logged;
    }, function (curr) {
        // Check user logged state
        if (curr) {
            vm.ui = getUIMenu(vm.user.data.type);
            navigateDefaultState(vm.user.data.type);
            return;
        }

        vm.ui = getUIMenu();
    });

    init();
});