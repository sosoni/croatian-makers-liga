CML.factory('mentors', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get mentors
    let get = function () {
        return $http.get(config.api + '/mentors/admin/list');
    };

    // Update mentors
    let update = function (data) {
        return $http.put(config.api + '/mentors/admin/update', data);
    };

    return {
        get: get,
        update: update
    };
});