CML.controller('MentorsCtrl', function (
    $filter,
    $log,
    CMLConfig,
    mentors,
    Notify
) {
    let vm = this;
    let config = CMLConfig;

    let init = function () {
        vm.config = config;
        vm.search = {
            name: ''
        };

        // Populate data
        mentors.get().then(function (response) {
            vm.data = parseData(response.data);
            vm.ageGroups = response.data.mentorsAgeGroups;
            vm.page = vm.page || 1;

            vm.filters = getFilters();
            vm.sortBy(vm.sortType || vm.filters[0].key);
        });
    };

    // Parse response data
    let parseData = function (data) {
        return data.institutions.map(function (institution) {
            institution.mentors = [];

            data.mentors.forEach(function (mentor) {
                if (mentor.institutionId === institution.id) {
                    institution.mentors.push(mentor);
                }
            });
            return institution;
        });
    };

    // Get predefined filters for ordering data
    let getFilters = function () {
        return [{
            name: 'Naziv ustanove',
            key: 'name'
        }, {
            name: 'OIB',
            key: 'oib'
        }];
    };

    // Update data of modal object
    vm.updateModalData = function (institution) {
        vm.modal = institution;
    };

    // Sort data by filter type
    vm.sortBy = function (type, reverse) {
        vm.sortType = type;
        vm.data = $filter('orderBy')(vm.data, type, reverse);
    };

    // Post data to database
    vm.postData = function () {
        mentors.update(vm.modal.mentors).then(function (response) {
            if (response.data) {
                Notify.success('Uspješno ste editirali mentore.');
                $log.debug('Editing mentors: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja mentora!');
            $log.warn(err);
        });
        vm.modal = null;
    };

    init();
});