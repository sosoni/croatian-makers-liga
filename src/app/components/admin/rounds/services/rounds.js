CML.factory('rounds', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Create rounds
    let create = function (data) {
        return $http.post(config.api + '/rounds/admin/create', data);
    };

    // Get rounds
    let get = function () {
        return $http.get(config.api + '/rounds/admin/list');
    };

    // Update rounds
    let update = function (data) {
        return $http.put(config.api + '/rounds/admin/update', data);
    };

    return {
        create: create,
        get: get,
        update: update
    };
});