CML.controller('RoundsCtrl', function (
    $filter,
    $log,
    $state,
    CMLConfig,
    Notify,
    rounds
) {
    let vm = this;
    let config = CMLConfig;

    let init = function () {
        vm.config = config;
        vm.search = {
            name: '',
            round: {
                name: '',
                key: ''
            }
        };

        // Populate data
        rounds.get().then(function (response) {
            vm.data = response.data.rounds;
            vm.page = vm.page || 1;

            vm.rounds = response.data.roundsTypes;
            vm.filters = getFilters();
            vm.sortBy(vm.sortType || vm.filters[0].key);
        });
    };

    // Get predefined filters for ordering data
    let getFilters = function () {
        return [{
            name: 'Naziv',
            key: 'name'
        }, {
            name: 'Status',
            key: 'opened'
        }];
    };

    // Get predefined rounds for filter
    let getRounds = function (data) {
        return [{
            name: 'Sve',
            key: ''
        }, {
            name: 'Online kola',
            key: 'online'
        }, {
            name: 'Fizička kola',
            key: 'physical'
        }];
    };

    // Update data of modal object
    vm.updateModalData = function (type, round) {
        switch (type) {
            case 'create':
                vm.modal = {
                    name: '',
                    created: new Date(),
                    opened: true,
                    type: {
                        id: 1,
                        type: 'online'
                    },
                    googleSheets: ''
                };
                break;
            case 'edit':
                vm.modal = round;
                break;
        }
    };

    // Check if any round is open
    vm.isAnyRoundOpen = function () {
        if (!vm.data) return false;

        return vm.data.find(function(round) {
            return round.opened;
        });
    };

    // Sort data by filter type
    vm.sortBy = function (type, reverse) {
        vm.sortType = type;
        vm.data = $filter('orderBy')(vm.data, type, reverse);
    };

    // Post data to database
    vm.postData = function () {
        rounds.update(vm.modal).then(function (response) {
            if (response.data) {
                Notify.success('Uspješno ste editirali kolo.');
                $log.debug('Updating round: ', response);
            }
            vm.modal = null;
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja kola!');
            $log.warn(err);
        });
    };

    // Create round in db
    vm.createData = function () {
        rounds.create(vm.modal).then(function (response) {
            if (response.data) {
                // Add missing props
                vm.modal.id = response.data.roundId;

                // Add/sort created round to data
                vm.data.push(vm.modal);
                vm.sortBy(vm.filters[0].key);

                Notify.success('Uspješno ste kreirali kolo.');
                $log.debug('Creating round: ', response);
            }
            vm.modal = null;
        }).catch(function (err) {
            Notify.warn('Greška prilikom kreiranja kola!');
            $log.warn(err);
        });

    };

    init();
});