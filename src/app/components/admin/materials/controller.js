CML.controller('AdminMaterialsCtrl', function (
    $timeout,
    $log,
    CMLMaterialsSvc,
    CMLModalSrv,
    CMLConfig,
    Notify,
    institutions
) {
    let vm = this;

    let init = function () {
        vm.config = CMLConfig;
        vm.page = vm.page || 1;

        vm.uploadedMaterials = [];
        vm.selectedMaterial = null;
        vm.visibility = {};
        vm.visibilityList = [];
        vm.institutions = [];
        vm.materialVisibilityDetails = [];
        vm.dropdownTranslations = {
            checkAll: 'Odaberi sve',
            uncheckAll: 'Ukloni sve',
            buttonDefaultText: 'Odaberi',
            selectionCount: 'Selektiran'
        };
        vm.extraSettings = {
            displayProp: 'name'
        };

        getMaterials();
        getInstitutions();
    };

    function getMaterials() {
        CMLMaterialsSvc.getMaterials().then(function (response) {
            vm.uploadedMaterials = response.data;
        });
    }

    function getInstitutions() {
        institutions.get().then(function (response) {
            vm.institutions = response.data;
        });
    }

    vm.fileIsImg = function (item) {
        return /^image/mig.test(item.type);
    };

    vm.deleteMaterial = function (material) {
        CMLMaterialsSvc.deleteMaterial(material.id).then(function () {
            getMaterials();
            Notify.success('Datoteka uspješno pobrisana.');
        }, function (err) {
            Notify.warn('Greška pri brisanju datoteke.');
            $log.warn(err);
        });
    };

    vm.resetMaterials = function () {
        vm.files = [];
    };

    vm.modifyVisibility = function (file) {
        // Create shallow copy
        var visiblity = angular.copy(vm.visibilityList);
        vm.visibility[file.name] = [];
        for (var i = 0; i < visiblity.length; i++) {
            vm.visibility[file.name].push(visiblity[i].id);
        }
        vm.visibility[file.name] = vm.visibility[file.name].join();
        // Clear list
        vm.visibilityList = [];
    };

    vm.populateDropdown = function (material) {
        vm.materialVisibilityDetails = [];
        var visibility = material.visibility.split(',');
        for (var i = 0; i < visibility.length; i++) {
            if (visibility[i] !== "0" && Boolean(visibility)) {
                vm.materialVisibilityDetails.push({
                    id: parseInt(visibility[i])
                });
            }
        }
    };

    // Get file size in MB
    vm.getFileSize = function (sizeInBytes) {
        return (sizeInBytes / (1000 * 1024)).toFixed(2);
    };

    vm.setSelectedMaterial = function (material) {
        vm.selectedMaterial = material;
    };

    vm.uploadMaterial = function () {
        if (vm.files.length < 1) {
            return false;
        }
        CMLMaterialsSvc.uploadMaterial(vm.files, vm.visibility).then(function (response) {
            vm.resetMaterials();
            vm.visibility = [];
            getMaterials();
            Notify.success('Datoteke uspješno spremljene.');
        }, function (err) {
            Notify.warn('Greška pri spremanju datoteka.');
            $log.warn(err);
        });
    };

    vm.downloadMaterial = function (material) {
        CMLMaterialsSvc.downloadMaterial(material.id).then(function (response) {
            var file = new Blob([response.data], { type: material.mime_type });
            saveAs(file, material.name);
        }, function (err) {
            Notify.warn('Greška pri preuzimanju datoteke.');
            $log.warn(err);
        });
    };

    vm.updateMaterialVisibility = function (material) {
        var visibilityArr = [],
            visibility;

        for (var i = 0; i < vm.materialVisibilityDetails.length; i++) {
            visibilityArr.push(vm.materialVisibilityDetails[i].id);
        }
        if (visibilityArr.length) {
            visibility = visibilityArr.join();
        }

        CMLMaterialsSvc.updateMaterial(material.id, visibility).then(function (response) {
            getMaterials();
            CMLModalSrv.closeModal('edit-material-modal');
            Notify.success('Detalji uspješno ažurirani.');
        }, function (err) {
            Notify.warn('Greška pri ažuriranju datoteke.');
            $log.warn(err);
        });
    };

    init();
});