CML.service('CMLMaterialsSvc', function (
    $http,
    CMLConfig,
    Upload
) {

    let config = CMLConfig;

    return {
        uploadMaterial : function(file, visibility) {
            return Upload.upload({
                url: config.api + '/materials/upload',
                data: {file: file,  visibility : visibility}
            });
        },

        getMaterials : function(params) {
            return $http({
                url: config.api + '/materials/all',
                method : 'GET',
                params: params
            });
        },

        deleteMaterial : function (id) {
            return $http.delete(config.api + '/materials/' + id);
        },

        downloadMaterial : function(id) {
            return $http.get(config.api + '/materials/download/' + id, {responseType: "arraybuffer"});
        },

        updateMaterial : function (id, visibility) {
            return $http.put(config.api + '/materials/' + id, {visibility : visibility});
        }
    };
});