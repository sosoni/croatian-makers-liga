CML.controller('CompetitorsCtrl', function (
    $filter,
    $log,
    competitors,
    CMLConfig,
    CMLModalSrv,
    Notify
) {
    let vm = this;
    let config = CMLConfig;

    let init = function () {
        vm.config = config;
        vm.user = {};
        vm.search = {
            name: '',
            ageGroup: ''
        };

        // Populate data
        competitors.get().then(function (response) {
            vm.data = parseData(response.data);
            vm.ageGroups = response.data.competitorsAgeGroups;
            vm.page = vm.page || 1;

            vm.filters = getFilters();
            vm.sortBy(vm.sortType || vm.filters[0].key);
        });
    };

    // Parse response data
    let parseData = function (data) {
        return data.competitors.map(function (competitor) {
            competitor.institution = data.institutions.find(function (institution) {
                return institution.id === competitor.institutionId;
            });

            competitor.mentors = data.mentors.filter(function (mentor) {
                return mentor.institutionId === competitor.institutionId;
            });

            competitor.birthdate = new Date(competitor.birthdate);
            competitor.institutionName = competitor.institution.name;

            return competitor;
        });
    };

    // Get predefined filters for ordering data
    let getFilters = function () {
        return [{
            name: 'Ime',
            key: 'name'
        }, {
            name: 'Prezime',
            key: 'lastname'
        }, {
            name: 'Razred',
            key: 'class'
        }, {
            name: 'Spol',
            key: 'sex'
        }, {
            name: 'Verificiran',
            key: 'approved'
        }];
    };

    vm.updateUserData = function (id) {
        let user = $filter('filter')(vm.data, { id: id }, true);
        vm.user = user[0];
    };

    // Sort data by filter type
    vm.sortBy = function (type, reverse) {
        vm.sortType = type;
        vm.data = $filter('orderBy')(vm.data, type, reverse);
    };

    vm.updateUser = function () {
        competitors.update(vm.user).then(function (response) {
            if (response.data) {
                CMLModalSrv.closeModal('edit-competitor');
                Notify.success('Uspješno ste editirali natjecatelja.');
                $log.debug('Editing competitors: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja natjecatelja!');
            $log.warn(err);
        });
    };

    init();
});