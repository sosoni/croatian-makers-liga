CML.factory('competitors', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get mentors
    let get = function () {
        return $http.get(config.api + '/competitors/admin/list');
    };

    // Update mentors
    let update = function (data) {
        return $http.put(config.api + '/competitors/admin/update', data);
    };

    let downloadDocument = function(name) {
        return $http.get(config.api + '/competitors/download/' + name, {responseType: "arraybuffer"});
    };

    return {
        get: get,
        downloadDocument : downloadDocument,
        update: update
    };
});