CML.factory('institutions', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get institutions
    let get = function () {
        return $http.get(config.api + '/institutions/list');
    };

    // Update institutions
    let update = function (data) {
        return $http.put(config.api + '/institutions/update', data);
    };

    // Register institution
    let register = function (data) {
        return $http.post(config.api + '/institutions/create', data);
    };

    return {
        get: get,
        update: update,
        register: register
    };
});