CML.controller('InstitutionsCtrl', function (
    $filter,
    $log,
    $timeout,
    CMLConfig,
    institutions,
    Notify
) {
    let config = CMLConfig;
    let vm = this;

    let init = function () {
        vm.config = config;
        vm.search = {
            name: '',
            regionalCenter: undefined
        };

        getData();
    };

    let getData = function () {
        // Populate data
        institutions.get().then(function (response) {
            vm.data = getParsedData(response.data);
            vm.page = vm.page || 1;

            vm.regionalCenters = getRegionalCenters(vm.data);
            vm.filters = getFilters();
            vm.sortBy(vm.sortType || vm.filters[0].key);
        });
    };

    let getParsedData = function (institutions) {
        institutions.forEach(function (institution) {
            institution.authorized = institution.user.authorized;
        });

        return institutions;
    };

    // Get predefined filters for ordering data
    let getFilters = function () {
        return [{
            name: 'Naziv',
            key: 'name'
        }, {
            name: 'Grad/Mjesto',
            key: 'city'
        }, {
            name: 'Regionalni centar',
            key: 'isRegionalCenter'
        }, {
            name: 'Zainteresiran za organizaciju',
            key: 'isInterestedInOrganisation'
        }, {
            name: 'Verificiran',
            key: 'authorized'
        }];
    };

    // Get regional centers from data
    let getRegionalCenters = function (institutions) {
        let regionalCenters = [];

        institutions.forEach(function (institution) {
            if (institution.isRegionalCenter && regionalCenters.indexOf(institution.regionalCenterName) === -1) {
                regionalCenters.push(institution.regionalCenterName);
            }
        });

        return regionalCenters;
    };

    // Update data of modal object
    vm.updateModalData = function (institution) {
        vm.modal = institution;
    };

    // Sort data by filter type
    vm.sortBy = function (type, reverse) {
        vm.sortType = type;
        vm.data = $filter('orderBy')(vm.data, type, reverse);
    };

    // Post data to database
    vm.postData = function () {
        institutions.update(vm.modal).then(function (response) {
            if (response.data) {
                vm.modal = null;
                getData();
                Notify.success('Uspješno ste editirali instituciju.');
                $log.debug('Editing institutions: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja institucije!');
            $log.warn(err);
        });
    };

    init();
});