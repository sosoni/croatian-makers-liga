CML.factory('adminResults', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    // Get results
    let get = function () {
        return $http.get(config.api + '/results/list');
    };

    // Update results
    let update = function (data) {
        return $http.put(config.api + '/results/update', data);
    };

    // Create results
    let create = function (data) {
        return $http.post(config.api + '/results/create', data);
    };

    return {
        get: get,
        update: update,
        create: create
    };
});