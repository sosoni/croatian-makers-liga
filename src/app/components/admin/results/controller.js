CML.controller('AdminResultsCtrl', function (
    $log,
    $scope,
    adminResults,
    CMLConfig,
    CMLModalSrv,
    Notify
) {
    let vm = this;
    let config = CMLConfig;

    let init = function () {
        vm.config = config;
        vm.search = { name: '' };

        getResults();
    };

    let getResults = function () {
        adminResults.get().then(function (response) {
            vm.data = response.data;
        });
    };

    vm.updateModalData = function (type, result) {
        switch (type) {
            case 'create':
                vm.modal = {};
                break;
            case 'edit':
                vm.modal = result;
                break;
        }
    };

    vm.updateData = function () {
        if ($scope.creationForm.$invalid) {
            $scope.creationForm.$setSubmitted();
            return false;
        }

        adminResults.update(vm.modal).then(function (response) {
            if (response.data) {
                getResults();
                CMLModalSrv.closeModal('edit-modal');
                Notify.success('Uspješno ste editirali rezultate.');
                $log.debug('Updating mentors: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom editiranja rezultata!');
            $log.warn(err);
        });
        vm.modal = null;
    };

    vm.createData = function () {
        if ($scope.creationForm.$invalid) {
            $scope.creationForm.$setSubmitted();
            return false;
        }

        adminResults.create(vm.modal).then(function (response) {
            if (response.data) {
                getResults();
                CMLModalSrv.closeModal('create-modal');
                Notify.success('Uspješno ste dodali rezultat.');
                $log.debug('Creating results: ', response);
            }
        }).catch(function (err) {
            Notify.warn('Greška prilikom dodavanja rezultata!');
            $log.warn(err);
        });
    };

    init();
});