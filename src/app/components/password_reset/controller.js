CML.controller('PasswordResetCtrl', function (
    $state,
    CMLUserSvc,
    data
) {
    let vm = this;

    vm.discard = function () {
        $state.go('home');
    };

    vm.reset = function () {
        CMLUserSvc.setNewPassword(data, md5(vm.password)).then(function () {
            // Show notification
        }).finally(function () {
            $state.go('home');
        });
    };
});