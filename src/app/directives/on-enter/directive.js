CML.directive('onEnter', function () {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            elem.bind('keydown, keypress', function (e) {
                if (e.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEnter, { 'event': e });
                    });
                }
            });
        }
    };

});