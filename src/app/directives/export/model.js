// Model for `cmlExport` directive
CML.factory('exportModel', function () {

    // Institution constructor
    function Institution(data) {
        this['Korisnicki email'] = data.user.email;
        this.id = data.id;
        this.ustanova = data.name;
        this.oib = data.oib;
        this.adresa = data.address;
        this.grad = data.city + ' (' + data.postcode + ')';
        this.direktor = data.director + ' (' + data.directorPhone +')';
        this.direktorEmail = data.directorEmail;
        this['regionalni centar'] = data.isRegionalCenter ? 'DA' : 'NE';
        this['zainteresiran za organizaciju'] = data.isInterestedInOrganisation ? 'DA' : 'NE';
        this['naziv regionalnog centra'] = data.regionalCenterName ? data.regionalCenterName : '-';
    }

    // Mentor constructor
    function Mentor(data) {
        this.id = data.institutionId;
        this.ustanova = data.institution;
        this['ime i prezime'] = data.name;
        this['mail mentora'] = data.email;
        this.telefon = data.phone;
        this['dobna skupina'] = data.ageGroup.displayName;
    }

    // Competitor constructor
    function Competitor(data) {
        this.id = data.id;
        this.ustanova = data.institution.name;
        this['ime i prezime'] = data.name + ' ' + data.lastname;
        this.spol = data.sex;
        this['datum rodjenja'] = new Date(data.birthdate).toLocaleDateString();
        this.razred = data.class;
        this['dobna skupina'] = data.ageGroup.id === 1 ? 'Mlađi (1. - 4.)' : 'Stariji (5. - 8.)';
        this.verificiran = data.approved ? 'DA' : 'NE';
    }

    // Round constructor
    function Round(data) {
        this.ime = data.name;
        this['datum nastanka'] = new Date(data.created).toLocaleDateString();
        this.otvoreno = data.opened ? 'DA' : 'NE';
    }

    // Result constructor
    function Result(data) {
        this.id = data.id;
        this.ustanova = data.institution.name;
        this['ime i prezime'] = data.name + ' ' + data.lastname;
        this['datum rodjenja'] = new Date(data.birthdate).toLocaleDateString();
        this.razred = data.class;
        this.verificiran = data.approved ? 'DA' : 'NE';

        data.rounds.forEach(function (round) {
            this['rez. - ' + round.name] = round.result;
        }, this);
    }

    return {
        Institution: Institution,
        Mentor: Mentor,
        Competitor: Competitor,
        Round: Round,
        Result: Result
    };
});