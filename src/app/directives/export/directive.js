CML.directive('cmlExport', function (
    exportModel
) {
    let ExportConstructor = exportModel;

    return {
        restrict: 'E',
        templateUrl: 'app/directives/export/view.html',
        scope: {
            data: '=',
            type: '@'
        },
        link: function (scope, elem) {
            // Export CSV file
            function exportToCsvFile(data, fileName) {
                // Parse JSON to CSV
                function parseJSONToCSVStr(data) {
                    if (data.length === 0) return;

                    let delimiter = {
                        column: ',',
                        line: '\n'
                    };
                    let keys = Object.keys(data[0]);

                    // Format to csv
                    let csvHeader = keys.map(function (item) {
                        // Capitalize headers
                        return item.charAt(0).toUpperCase() + item.slice(1);
                    }).join(delimiter.column);
                    let csvStr = csvHeader + delimiter.line;
                    data.forEach(function (item) {
                        keys.forEach(function (key, i) {
                            if ((i > 0) && (i < keys.length)) {
                                csvStr = csvStr + delimiter.column;
                            }
                            csvStr = csvStr + item[key];
                        });
                        csvStr = csvStr + delimiter.line;
                    });

                    return encodeURIComponent(csvStr);
                }

                let csvStr = parseJSONToCSVStr(data);
                let dataUri = 'data:text/csv;charset=utf-8,' + csvStr;
                let exportFileDefaultName = (fileName || 'data') + '.csv';

                // Download data
                let linkElement = document.createElement('a');
                linkElement.setAttribute('href', dataUri);
                linkElement.setAttribute('download', exportFileDefaultName);
                linkElement.click();
            }

            // Data export handler
            function handleDataExport(data, type) {
                let dataToExport;
                let fileName;

                switch (type) {
                    case 'institutions':
                        fileName = 'institucije';
                        dataToExport = data.map(function (institution) {
                            return new ExportConstructor.Institution(institution);
                        }).sort(function (a, b) {
                            if (a.ustanova < b.ustanova) return -1;
                            if (a.ustanova > b.ustanova) return 1;
                            return 0;
                        });
                        break;
                    case 'mentors':
                        fileName = 'mentori';
                        dataToExport = [];
                        data.forEach(function (institution) {
                            institution.mentors.forEach(function (mentor) {
                                mentor.institutionId = institution.id;
                                mentor.institution = institution.name;
                                dataToExport.push(new ExportConstructor.Mentor(mentor));
                            });
                        });
                        dataToExport.sort(function (a, b) {
                            if (a.ustanova < b.ustanova) return -1;
                            if (a.ustanova > b.ustanova) return 1;
                            return 0;
                        });
                        break;
                    case 'competitors':
                        fileName = 'natjecatelji';
                        dataToExport = data.map(function (competitor) {
                            return new ExportConstructor.Competitor(competitor);
                        }).sort(function (a, b) {
                            if (a.ustanova < b.ustanova) return -1;
                            if (a.ustanova > b.ustanova) return 1;
                            return 0;
                        });
                        break;
                    case 'rounds':
                        fileName = 'kola';
                        dataToExport = data.map(function (round) {
                            return new ExportConstructor.Round(round);
                        }).sort(function (a, b) {
                            if (a.name < b.name) return -1;
                            if (a.name > b.name) return 1;
                            return 0;
                        });
                        break;
                    case 'results':
                        fileName = 'rezultati';
                        dataToExport = data.map(function (round) {
                            return new ExportConstructor.Result(round);
                        }).sort(function (a, b) {
                            if (a.ustanova < b.ustanova) return -1;
                            if (a.ustanova > b.ustanova) return 1;
                            return 0;
                        });
                        break;
                }

                // Export data
                exportToCsvFile(dataToExport, fileName);
            }

            // Export data
            scope.exportData = function () {
                if (!scope.data) return;

                handleDataExport(JSON.parse(JSON.stringify(scope.data)), scope.type);
            };
        }
    };
});