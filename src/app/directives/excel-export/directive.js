CML.directive('excelExport', function (
    $http,
    CMLConfig
) {
    let config = CMLConfig;

    let download = function (data, filename) {
        let blob = new Blob([data], {
            type: 'application/vnd.ms-excel'
        });
        let objectUrl = window.URL.createObjectURL(blob);

        let exportFileDefaultName = (filename || 'data') + '.xlsx';
        var link = document.createElement('a');
        document.body.appendChild(link);
        link.setAttribute('href', objectUrl);
        link.setAttribute('download', exportFileDefaultName);
        link.click();

        // Solve download in FireFox
        setTimeout(function() {
            window.URL.revokeObjectURL(objectUrl);
        }, 10);
    };

    return {
        restrict: 'E',
        templateUrl: 'app/directives/excel-export/view.html',
        scope: {
            data: '=',
            type: '@'
        },
        link: function (scope, elem) {
            scope.exportData = function () {
                return $http({
                    method: 'GET',
                    url: config.api + '/excel/export',
                    params: {
                        type: scope.type,
                        id: scope.data ? scope.data.id : null
                    },
                    responseType: 'arraybuffer'
                }).then(function (response) {
                    let data = response.data;
                    if (data) download(data, scope.type);
                });
            };
        }
    };
});