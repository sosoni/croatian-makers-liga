CML.directive('cmlSupport', function (
    $http,
    $log,
    CMLConfig
) {
    let config = CMLConfig;

    return {
        restrict: 'E',
        templateUrl: 'app/directives/support/view.html',
        scope: {},
        link: function (scope) {
            // Submit form
            scope.submit = function () {
                let data = {
                    to: config.support.mailTo,
                    from: scope.modal.emailFrom,
                    subject: scope.modal.subject,
                    text: scope.modal.text
                };

                $http.post(config.api + '/emailer-support', data).then((response) => {
                    $log.info('Mail sent:', response);
                });
                scope.modal = null;
            };
        }
    };
});