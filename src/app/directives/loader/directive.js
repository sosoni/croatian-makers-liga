CML.directive('cmlLoader', function (
    $http
) {
    return {
        restrict: 'E',
        templateUrl: 'app/directives/loader/view.html',
        link: function (scope, elem, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function(state) {
                if (state) elem.show();
                else elem.hide();
            });
        }
    };
});