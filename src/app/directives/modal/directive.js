CML.directive('cmlModal', function () {
    return {
        restrict: 'E',
        templateUrl: function (tElem, tAttrs) {
            return tAttrs.templatePath;
        },
        scope: {
            modal: '=data',
            onSubmit: '&',
            onCancel: '&'
        },
        link: function (scope, elem, attrs) {
            // Submit form
            scope.submit = function () {
                scope.onSubmit();
            };

            // Cancel form submit
            scope.cancel = function () {
                scope.onCancel();
            };
        }
    };
});


CML.directive('modal', function (CMLModalSrv) {
    return {
        restrict: 'E',
        templateUrl: function (tElem, tAttrs) {
            return tAttrs.templatePath;
        },
        replace: true,
        link: function (scope, el, attr) {
            CMLModalSrv.addModal({ scope: scope, el: el, attr: attr });

            scope.closeModal = function () {
                $('.modal').modal('hide');
            };
        }
    };
});