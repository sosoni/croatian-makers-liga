CML.service('CMLModalSrv', function () {
    let modal = {};

    return {

        addModal : function(data){
            modal[data.attr.id] = data;
        },

        closeModal: function(id) {
            modal[id].scope.closeModal();
        }
    };
});