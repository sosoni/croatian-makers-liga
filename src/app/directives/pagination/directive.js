CML.directive('cmlPagination', function (
    $window,
    CMLConfig
) {
    let config = CMLConfig;

    return {
        restrict: 'E',
        templateUrl: 'app/directives/pagination/view.html',
        scope: {
            total: '=total',
            page: '=page'
        },
        link: function (scope) {
            scope.totalPages = Math.ceil(scope.total / config.paginationItemsLimit);
            var initPages = function () {
                scope.currentPage = scope.page || 1;

                if (scope.totalPages <= 5) {
                    scope.startPage = 1;
                    scope.endPage = scope.totalPages;
                } else {
                    if (scope.currentPage <= 3) {
                        scope.startPage = 1;
                        scope.endPage = 5;
                    } else if (scope.currentPage + 2 >= scope.totalPages) {
                        scope.startPage = scope.totalPages - 4;
                        scope.endPage = scope.totalPages;
                    } else {
                        scope.startPage = scope.currentPage - 2;
                        scope.endPage = scope.currentPage + 2;
                    }
                }


                scope.pages = [];
                for (let i = scope.startPage; i < scope.endPage + 1; i++) {
                    scope.pages.push(i);
                }
            };

            scope.handlePageChange = function (page) {
                if (page < 1) {
                    scope.page = 1;
                    return;
                } else if (page > scope.totalPages) {
                    scope.page = scope.totalPages;
                    return;
                }

                scope.page = page;
                initPages();
                $window.scrollTo(0, 0);
            };

            initPages();
        }
    };
});