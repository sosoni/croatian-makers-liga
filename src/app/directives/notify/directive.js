CML.directive('cmlNotify', function (
    Notify
) {
    return {
        restrict: 'E',
        templateUrl: 'app/directives/notify/view.html',
        link: function (scope, elem) {
            scope.queue = Notify.queue;

            // Remove from queue 
            scope.close = Notify.remove;
        }
    };
});