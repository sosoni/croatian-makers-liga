CML.factory('Notify', function (
    $timeout
) {
    let hideDelay = 5000;
    let queue = [];

    // Add item to queue
    let addToQueue = function (item) {
        // Run digest
        $timeout(function () {
            queue.push(item);
        });

        // Hide 
        $timeout(function () {
            queue.shift();
        }, hideDelay);
    };

    return {
        queue: queue,
        success: function (info) {
            addToQueue({
                type: 'success',
                info: info
            });
        },
        info: function (info) {
            addToQueue({
                type: 'info',
                info: info
            });
        },
        warn: function (info) {
            addToQueue({
                type: 'warning',
                info: info
            });
        },
        danger: function (info) {
            addToQueue({
                type: 'danger',
                info: info
            });
        },
        remove: function (index) {
            // Run digest
            $timeout(function () {
                queue.splice(index, 1);
            });
        }
    };

});