const CML = angular.module('CML', [
    'ui.router',
    'angular-locker',
    'ngFileUpload',
    'angularjs-dropdown-multiselect',
    'angular-loading-bar'
]);

// Main config
CML.config(function (
    $compileProvider,
    $locationProvider,
    $logProvider,
    $stateProvider,
    $httpProvider,
    $urlRouterProvider,
    lockerProvider,
    cfpLoadingBarProvider,
    CMLSettings
) {
    const settings = CMLSettings;

    // Optimize
    $logProvider.debugEnabled(settings.debug);
    $compileProvider.debugInfoEnabled(settings.debug);
    $compileProvider.commentDirectivesEnabled(false);
    $compileProvider.cssClassDirectivesEnabled(false);


    // Configure loader
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.latencyThreshold = 0;

    // Configure locker
    lockerProvider.defaults({
        driver: 'local',
        namespace: 'cml',
        separator: '.'
    });

    // Remove hash prefix
    $locationProvider.hashPrefix('');

    // Default route
    $urlRouterProvider.otherwise('/');

    // Routes
    $stateProvider
        .state('base', {
            controller: 'BaseCtrl',
            controllerAs: 'base',
            templateUrl: 'app/components/base/view.html',
            abstract: true
        })
        .state('home', {
            url: '/',
            parent: 'base',
            controller: 'HomeCtrl as home',
            templateUrl: 'app/components/home/view.html'
        })
        .state('password-reset', {
            url: '/password-reset?uuid',
            parent: 'base',
            controller: 'PasswordResetCtrl as passwordReset',
            templateUrl: 'app/components/password_reset/view.html',
            resolve: {
                data: function ($state, $stateParams, CMLUserSvc) {
                    var uuid = $stateParams.uuid;
                    return CMLUserSvc.checkPasswordReset(uuid)
                        .then(function (response) {
                            return response.data;
                        }).catch(function () {
                            //If pass reset isn't required already prevent going to the route
                            $state.go('home');
                        });
                }
            }
        })
        // Admin states
        .state('admin-institutions', {
            url: '/admin/institutions',
            parent: 'base',
            controller: 'InstitutionsCtrl as institutions',
            templateUrl: 'app/components/admin/institutions/view.html',
            data: {
                auth: 'admin'
            }
        })
        .state('admin-mentors', {
            url: '/admin/mentors',
            parent: 'base',
            controller: 'MentorsCtrl as mentors',
            templateUrl: 'app/components/admin/mentors/view.html',
            data: {
                auth: 'admin'
            }
        })
        .state('admin-competitors', {
            url: '/admin/competitors',
            parent: 'base',
            controller: 'CompetitorsCtrl as competitors',
            templateUrl: 'app/components/admin/competitors/view.html',
            data: {
                auth: 'admin'
            }
        })
        .state('admin-rounds', {
            url: '/admin/rounds',
            parent: 'base',
            controller: 'RoundsCtrl as rounds',
            templateUrl: 'app/components/admin/rounds/view.html',
            data: {
                auth: 'admin'
            }
        })
        .state('admin-materials', {
            url: '/admin/materials',
            parent: 'base',
            controller: 'AdminMaterialsCtrl as adminMaterials',
            templateUrl: 'app/components/admin/materials/view.html',
            data: {
                auth: 'admin'
            }
        })
        .state('admin-results', {
            url: '/admin/results',
            parent: 'base',
            controller: 'AdminResultsCtrl as results',
            templateUrl: 'app/components/admin/results/view.html',
            data: {
                auth: 'admin'
            }
        })

        // User states
        .state('user-institution', {
            url: '/user/institution',
            parent: 'base',
            controller: 'UserInstitutionCtrl as userInstitution',
            templateUrl: 'app/components/user/institution/view.html',
            data: {
                auth: 'user'
            }
        })
        .state('user-mentors', {
            url: '/user/mentors',
            parent: 'base',
            controller: 'UserMentorsCtrl as userMentors',
            templateUrl: 'app/components/user/mentors/view.html',
            data: {
                auth: 'user'
            }
        })
        .state('user-competitors', {
            url: '/user/competitors',
            parent: 'base',
            controller: 'UserCompetitorsCtrl as userCompetitors',
            templateUrl: 'app/components/user/competitors/view.html',
            data: {
                auth: 'user'
            }
        })
        .state('user-rounds', {
            url: '/user/rounds',
            parent: 'base',
            controller: 'UserRoundsCtrl as userRounds',
            templateUrl: 'app/components/user/rounds/view.html',
            data: {
                auth: 'user'
            }
        })
        .state('user-materials', {
            url: '/user/materials',
            parent: 'base',
            controller: 'UserMaterialsCtrl as userMaterials',
            templateUrl: 'app/components/user/materials/view.html',
            data: {
                auth: 'user'
            }
        })
        .state('user-results', {
            url: '/user/results',
            parent: 'base',
            controller: 'UserResultsCtrl as results',
            templateUrl: 'app/components/user/results/view.html',
            data: {
                auth: 'user'
            }
        })
        .state('otherwise', {
            url: '/'
        });

    // Add token interceptor
    $httpProvider.interceptors.push('tokenInterceptor');
});

CML.run(function (
    $location,
    $log,
    $rootScope,
    $state,
    CMLConfig,
    CMLUserSvc
) {
    const config = CMLConfig;

    // Set company name
    $rootScope.companyName = config.company.name;

    // Define open states
    const openStates = ['home', 'password-reset'];
    $rootScope.state = $state;

    var localUser = CMLUserSvc.getLocalUser();
    if (localUser) {
        CMLUserSvc.saveLocalUser(localUser);
    }

    $rootScope.$on('$stateChangeStart', function (event, toState, toParam, fromState, fromParam) {
        CMLUserSvc.isLogged().then(function () {
            if (!CMLUserSvc.checkProtectedRoute(toState, fromState)) {
                event.preventDefault();
                $state.go('home');
            }
        }).catch(function (err) {
            if (err.status === 401) {
                CMLUserSvc.clearLocalUser();
                if (openStates.indexOf(toState.name) === -1) $location.path('/');
            }
        });
    });
});

// Document ready event
angular.element(document).ready(function () {
    let init = function () {
        const settings = {
            debug: SETUP.debug,
            server: SETUP.server
        };

        const config = {
            api: SETUP.api.url[settings.server],
            company: SETUP.company,
            support: {
                enabled: SETUP.support.enabled,
                mailTo: SETUP.support.mailTo[settings.server]
            },
            paginationItemsLimit: SETUP.paginationItemsLimit
        };

        CML.constant('CMLSettings', settings);
        CML.constant('CMLConfig', config);

        // Bootstrap app
        angular.bootstrap(document, ['CML'], {});

        // Remove setup variable
        SETUP = angular.noop();
    };

    init();
});