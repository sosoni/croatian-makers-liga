// register the interceptor as a service
CML.factory('tokenInterceptor', function (
    $q,
    locker
) {
    return {
        request: function (request) {
            let token = locker.get('token');

            if (token) {
                request.headers.Authorization = token;
            }
            // do something on success
            return request;
        },

        response: function (response) {
            let token = response.headers('Access-Token');

            if (token) {
                locker.put('token', token);
            }
            // do something on success
            return response;
        },

        requestError: function (rejection) {
            // do something on error
            return $q.reject(rejection);
        },

        responseError: function (rejection) {
            // do something on error
            return $q.reject(rejection);
        }
    };
});
