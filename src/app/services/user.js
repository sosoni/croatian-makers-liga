CML.service('CMLUserSvc', function (
    $http,
    $rootScope,
    $state,
    $q,
    locker,
    CMLConfig
) {

    let config = CMLConfig;

    return {
        user: {
            data: null,
            logged: false
        },
        resetPassword: function (data) {
            return $http.post(config.api + '/password/reset', data);
        },
        checkPasswordReset: function (uuid) {
            return $http.get(config.api + '/password/reset_check', {
                params: { uuid: uuid }
            });
        },
        setNewPassword: function (data, password) {
            return $http.post(config.api + '/password/set_new', { data: data, password: password });
        },
        saveLocalUser: function (data) {
            locker.put('user', data);
            this.user.data = data;
            this.user.logged = true;
        },
        clearLocalUser: function () {
            locker.forget('token');
            locker.forget('user');
            this.user.data = null;
            this.user.logged = false;
        },
        getLocalUser: function () {
            let user = locker.get('user');
            return user;
        },
        checkProtectedRoute: function (stateTo, stateFrom) {
            if (!stateTo.data || !stateTo.data.auth) {
                return true;
            }

            if (this.user.data) {
                return stateTo.data.auth === this.user.data.type;
            }
        },
        getAllUsers: function () {
            return $http(config.api + '/auth/login_check');
        },
        isLogged: function () {
            return $http.get(config.api + '/auth/login_check');
        },
        login: function (data) {
            return $http.post(config.api + '/auth/login', data).then(function (response) {
                this.saveLocalUser(response.data);
                return $q.resolve(response);
            }.bind(this)).catch(function (err) {
                return $q.reject(err);
            });
        },
        logout: function () {
            return $http.get(config.api + '/auth/logout').then(function (response) {
                if (response.status === 200) {
                    this.clearLocalUser();
                    $state.go('home');
                    return true;
                } else {
                    return false;
                }
            }.bind(this));
        }
    };
});