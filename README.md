#Croatian Makers League

AngularJS application.

Steps to run client project:
---------------
###Client:
 * git clone
 * npm install
 * grunt dev

#Additional parameters
- grunt dev --server=dev --company=default --debug=true

Options `company`, `server` and `debug` are optional.

#Production build
- grunt dist --server=production --company=default --debug=false