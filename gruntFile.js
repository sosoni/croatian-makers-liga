module.exports = function (grunt) {
    // Load npm taskts
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-sass');

    // Import dependencies
    const extend = require('extend');

    // Init config
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Define settings
        settings: {
            server: {
                protocol: 'http',
                host: 'localhost',
                port: 3000
            },
            dirs: {
                src: 'src',
                dist: 'dist',
                npm: 'node_modules'
            },
            theme: {
                name: null
            }
        },

        // Connect task
        connect: {
            server: {
                options: {
                    debug: true,
                    protocol: '<%= settings.server.protocol %>',
                    hostname: '<%= settings.server.host %>',
                    port: '<%= settings.server.port %>',
                    open: '<%= settings.server.protocol %>://<%= settings.server.host %>:<%= settings.server.port %>'
                }
            }
        },

        // Concat task
        concat: {
            deps: {
                src: [
                    '<%= settings.dirs.npm %>/jquery/dist/jquery.js',
                    '<%= settings.dirs.npm %>/bootstrap-sass/assets/javascripts/bootstrap.js',
                    '<%= settings.dirs.npm %>/angular/angular.js',
                    '<%= settings.dirs.npm %>/angular-ui-router/release/angular-ui-router.js',
                    '<%= settings.dirs.npm %>/angular-locker/dist/angular-locker.js',
                    '<%= settings.dirs.npm %>/ng-file-upload/dist/ng-file-upload.js',
                    '<%= settings.dirs.npm %>/file-saver/FileSaver.js',
                    '<%= settings.dirs.npm %>/angularjs-dropdown-multiselect/dist/angularjs-dropdown-multiselect.min.js',
                    '<%= settings.dirs.npm %>/angular-loading-bar/src/loading-bar.js',
                    '<%= settings.dirs.npm %>/blueimp-md5/js/md5.js'
                ],
                dest: '<%= settings.dirs.src %>/js/deps.js'
            },
            app: {
                src: [
                    '<%= settings.dirs.src %>/app/functions/wrapper/_intro.js',
                    '<%= settings.dirs.src %>/app/setup.js',
                    '<%= settings.dirs.src %>/app/functions/*.js',
                    '<%= settings.dirs.src %>/app/app.js',
                    '<%= settings.dirs.src %>/app/services/*.js',
                    '<%= settings.dirs.src %>/app/directives/**/*.js',
                    '<%= settings.dirs.src %>/app/components/**/*.js',
                    '<%= settings.dirs.src %>/app/functions/wrapper/_outro.js'
                ],
                dest: '<%= settings.dirs.src %>/js/app.js'
            }
        },

        // Babel task
        babel: {
            dist: {
                options: {
                    sourceMap: false,
                    presets: ['es2015']
                },
                files: {
                    '<%= settings.dirs.dist %>/js/app.js': '<%= settings.dirs.src %>/js/app.js'
                }
            }
        },

        // Clean task
        clean: {
            dev: [
                '<%= settings.dirs.src %>/css/**/*'
            ],
            dist: [
                '<%= settings.dirs.dist %>/*'
            ]
        },

        // Copy task
        copy: {
            assets: {
                files: [{
                    expand: true,
                    cwd: '<%= settings.dirs.npm %>/bootstrap-sass/assets/fonts/bootstrap/',
                    src: ['**'],
                    dest: '<%= settings.dirs.src %>/scss/themes/default/assets/fonts/'
                }, {
                    expand: true,
                    cwd: '<%= settings.dirs.npm %>/font-awesome/fonts/',
                    src: ['**'],
                    dest: '<%= settings.dirs.src %>/scss/themes/default/assets/fonts/'
                }, {
                    expand: true,
                    cwd: '<%= settings.dirs.src %>/scss/themes/default/',
                    src: ['assets/**'],
                    dest: '<%= settings.dirs.src %>/css/'
                }, {
                    expand: true,
                    cwd: '<%= settings.dirs.src %>/scss/themes/<%= settings.theme.name %>/',
                    src: ['assets/**'],
                    dest: '<%= settings.dirs.src %>/css/'
                }]
            },
            assets_dist: {
                files: [{
                    expand: true,
                    cwd: '<%= settings.dirs.src %>/scss/themes/default/',
                    src: ['assets/**'],
                    dest: '<%= settings.dirs.dist %>/css/'
                }, {
                    expand: true,
                    cwd: '<%= settings.dirs.src %>/scss/themes/<%= settings.theme.name %>/',
                    src: ['assets/**'],
                    dest: '<%= settings.dirs.dist %>/css/'
                }]
            },
            app_dist: {
                files: [{
                    expand: true,
                    cwd: '<%= settings.dirs.src %>/',
                    src: [
                        'index.html',
                        'app/**/*.html',
                        '!app/**/*.js',
                        'js/*'
                    ],
                    dest: '<%= settings.dirs.dist %>'
                }]
            }
        },

        // Html minify task
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    conservativeCollapse: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= settings.dirs.dist %>',
                    src: '**/*.html',
                    dest: '<%= settings.dirs.dist %>'
                }]
            }
        },

        // JSHint task
        jshint: {
            options: {
                browser: true,
                esversion: 6,
                globals: {
                    module: false,
                    require: false,
                    console: false,
                    exports: false,
                    angular: false,
                    SETUP: true
                },
                ignores: [
                    '<%= settings.dirs.src %>/app/**/_*.js'
                ]
            },
            files: [
                'gruntFile.js',
                '<%= settings.dirs.src %>/app/**/*.js',
                '!<%= settings.dirs.src %>/app/**/_*.js'
            ]
        },

        // Annotate task
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            dist: {
                files: [
                    {
                        expand: true,
                        src: [
                            '<%= settings.dirs.dist %>/js/app.js'
                        ]
                    }
                ]
            }
        },

        // Sass task
        sass: {
            dev: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: true,
                    unixNewlines: true,
                    noCache: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= settings.dirs.src %>/scss/',
                    src: ['*.scss'],
                    dest: '<%= settings.dirs.src %>/css/',
                    ext: '.css'
                }]
            },
            dist: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: false,
                    unixNewlines: true,
                    noCache: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= settings.dirs.src %>/scss/',
                    src: ['*.scss'],
                    dest: '<%= settings.dirs.dist %>/css/',
                    ext: '.css'
                }]
            }
        },

        // Uglify task
        uglify: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= settings.dirs.dist %>',
                    src: '**/*.js',
                    dest: '<%= settings.dirs.dist %>'
                }]
            }
        },

        // Watch task
        watch: {
            options: {
                atBegin: true
            },
            concat: {
                files: '<%= concat.app.src %>',
                tasks: ['concat:app']
            },
            jshint: {
                files: '<%= jshint.files %>',
                tasks: ['jshint']
            },
            sass: {
                files: '<%= settings.dirs.src %>/scss/**/*.scss',
                tasks: ['sass:dev'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // Generate task
    grunt.registerTask('generate', function () {
        let company = grunt.option('company') || 'default';
        let server = grunt.option('server') || 'dev';
        let debug = grunt.option('debug') !== undefined ? grunt.option('debug') : true;

        // Read build file
        let build = grunt.file.readJSON('build/default.json');
        let buildCompany = grunt.file.readJSON('build/' + company + '.json');
        extend(true, build, buildCompany);

        // Update params
        build.debug = debug;
        build.server = server;

        // Set params from build file
        grunt.config.set('settings.theme.name', build.theme);

        // Write setup file
        let output = 'window.SETUP =' + JSON.stringify(build, null, 4) + ';';
        output = output.replace(/\"(.[^-{}\s]*?)\":/g, '$1:');
        grunt.file.write('src/app/setup.js', output);
    });

    // Theme task
    grunt.registerTask('theme', function () {
        let root = grunt.config.get('settings.dirs.src');
        let theme = grunt.config.get('settings.theme.name');

        // Write theme file
        grunt.file.write(root + '/scss/themes/_theme.scss', '@import "' + theme + '/theme";');
    });

    // Dev task
    grunt.registerTask('dev', [
        'generate',
        'theme',
        'concat:deps',
        'concat:app',
        'clean:dev',
        'copy:assets',
        'sass:dev',
        'connect',
        'watch'
    ]);

    // Distribution task
    grunt.registerTask('dist', [
        'generate',
        'theme',
        'clean:dist',
        'copy:assets_dist',
        'sass:dist',
        'jshint',
        'concat:deps',
        'concat:app',
        'copy:app_dist',
        'babel:dist',
        'ngAnnotate:dist',
        'uglify:dist',
        'htmlmin:dist'
    ]);
};